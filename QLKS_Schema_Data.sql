USE [master]
GO
/****** Object:  Database [QLKS]    Script Date: 12/28/2015 7:01:42 PM ******/
CREATE DATABASE [QLKS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QLKS', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\QLKS.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'QLKS_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\QLKS_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [QLKS] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QLKS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QLKS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QLKS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QLKS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QLKS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QLKS] SET ARITHABORT OFF 
GO
ALTER DATABASE [QLKS] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [QLKS] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [QLKS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QLKS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QLKS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QLKS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QLKS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QLKS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QLKS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QLKS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QLKS] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QLKS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QLKS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QLKS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QLKS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QLKS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QLKS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QLKS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QLKS] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [QLKS] SET  MULTI_USER 
GO
ALTER DATABASE [QLKS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QLKS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QLKS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QLKS] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [QLKS]
GO
/****** Object:  StoredProcedure [dbo].[sp_DSTP]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_DSTP]
AS
	SELECT DISTINCT ks.thanhPho
    FROM KhachSan ks

GO
/****** Object:  StoredProcedure [dbo].[sp_KiemTraCMNDTonTai]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_KiemTraCMNDTonTai] @_soCMND char(20), @kq INT OUTPUT

AS

	IF (EXISTS(SELECT * FROM KhachHang WHERE soCMND = @_soCMND))

		SET @kq = 1

	ELSE

		SET @kq = 0


GO
/****** Object:  StoredProcedure [dbo].[sp_KiemTraEmailTonTai]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_KiemTraEmailTonTai] @_email char(20), @kq INT OUTPUT

AS

	IF (EXISTS(SELECT * FROM KhachHang WHERE email = @_email))

		SET @kq = 1

	ELSE

		SET @kq = 0



GO
/****** Object:  StoredProcedure [dbo].[sp_KiemTraTaiKhoan]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_KiemTraTaiKhoan] @TenDangNhap CHAR(20), @MatKhau CHAR(50)
AS
	SELECT kh.maKH, kh.hoTen
	FROM KhachHang kh
	WHERE kh.tenDangNhap = @TenDangNhap AND kh.matKhau = @MatKhau

GO
/****** Object:  StoredProcedure [dbo].[sp_KiemTraUserNameTonTai]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_KiemTraUserNameTonTai] @_tenDangNhap char(20), @kq INT OUTPUT

AS

	IF (EXISTS(SELECT * FROM KhachHang WHERE tenDangNhap = @_tenDangNhap))

		SET @kq = 1

	ELSE

		SET @kq = 0


GO
/****** Object:  StoredProcedure [dbo].[sp_LayDanhSachPhongTheoKhachSan]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_LayDanhSachPhongTheoKhachSan] @_MaKS CHAR(10), @_inDate DATE, @_bookDate DATE
AS
	SELECT Data.maPhong, Data.soPhong, Data.loaiPhong, Data.donGia, Data.moTa
	FROM
	(SELECT P1.maPhong, P1.soPhong, P1.loaiPhong, LP1.donGia, LP1.moTa
	FROM Phong P1 JOIN TrangThaiPhong TT1 ON P1.maPhong = TT1.maPhong 
		 JOIN LoaiPhong LP1 ON P1.loaiPhong = LP1.maLoaiPhong
	WHERE LP1.maKS = @_MaKS AND DATEDIFF(d,TT1.ngay,@_bookDate) = 0 AND TT1.tinhTrang = N'còn trống'
	UNION
	SELECT P2.maPhong, P2.soPhong, P2.loaiPhong, LP2.donGia, LP2.moTa
	FROM DatPhong DP2 JOIN Phong P2 ON DP2.maPhong = P2.maPhong
		 JOIN LoaiPhong LP2 ON P2.loaiPhong = LP2.maLoaiPhong
	WHERE LP2.maKS = @_MaKS AND DATEDIFF(d,DP2.ngayTraPhong, @_inDate) >= 0 
	) AS Data
	WHERE Data.maPhong NOT IN
	(SELECT P3.maPhong
	FROM DatPhong DP3 JOIN Phong P3 ON DP3.maPhong = P3.maPhong
		 JOIN LoaiPhong LP3 ON P3.loaiPhong = LP3.maLoaiPhong
	WHERE LP3.maKS = @_MaKS AND DATEDIFF(d,DP3.ngayTraPhong, @_inDate) < 0 
	)
GO
/****** Object:  StoredProcedure [dbo].[sp_LayMaKhachHang]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_LayMaKhachHang] @_tenDangNhap CHAR(20), @_matKhau CHAR(50), @_maKH CHAR(10) OUTPUT
AS
	SET @_maKH = (  SELECT KH.maKH
					FROM KhachHang KH
					WHERE KH.tenDangNhap = @_tenDangNhap AND KH.matKhau = @_matKhau)

GO
/****** Object:  StoredProcedure [dbo].[sp_SoSao]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_SoSao]
AS
	SELECT DISTINCT ks.soSao
    FROM KhachSan ks
	ORDER BY ks.soSao
GO
/****** Object:  StoredProcedure [dbo].[sp_ThemDatPhong]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_ThemDatPhong] @_maDP CHAR(10), @_maPhong CHAR(10), @_maKH CHAR(10), @_ngayBatDau DATE, @_ngayTraPhong DATE, @_ngayDat DATE, @_donGia FLOAT
AS
	INSERT INTO DatPhong VALUES (@_maDP, @_maPhong, @_maKH, @_ngayBatDau, @_ngayTraPhong, @_ngayDat, @_donGia, N'', N'chưa xác nhận')

GO
/****** Object:  StoredProcedure [dbo].[sp_ThemKhachHang]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_ThemKhachHang] @_maKH char(10), @_hoTen nvarchar(50), @_tenDangNhap char(20), @_matkhau char(50), @_soCMND char(20), @_diaChi nvarchar(50), @_soDT char(20), @_moTa nvarchar(50), @_email char(20)

AS

	INSERT INTO KhachHang VALUES (@_maKH, @_hoTen, @_tenDangNhap, @_matkhau, @_soCMND, @_diaChi, @_soDT, @_moTa, @_email)


GO
/****** Object:  StoredProcedure [dbo].[sp_TimKS_ThP]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_TimKS_ThP] @ThanhPho NVARCHAR(20)
AS
	SELECT ks.maKS, ks.tenKS, ks.soSao, ks.thanhPho, ks.giaTB
    FROM KhachSan ks
	WHERE ks.thanhPho LIKE @ThanhPho

GO
/****** Object:  StoredProcedure [dbo].[sp_TimKS_ThP_GiaTB]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_TimKS_ThP_GiaTB] @ThanhPho NVARCHAR(20), @GiaThapNhat FLOAT, @GiaCaoNhat FLOAT
AS
	SELECT ks.maKS, ks.tenKS, ks.soSao, ks.thanhPho, ks.giaTB
    FROM KhachSan ks
	WHERE ks.thanhPho LIKE @ThanhPho AND ks.giaTB >= @GiaThapNhat AND ks.giaTB <= @GiaCaoNhat

GO
/****** Object:  StoredProcedure [dbo].[sp_TimKS_ThP_SoSao]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_TimKS_ThP_SoSao] @ThanhPho NVARCHAR(20), @SoSao float
AS
	SELECT ks.maKS, ks.tenKS, ks.soSao, ks.thanhPho, ks.giaTB
    FROM KhachSan ks
	WHERE ks.thanhPho LIKE @ThanhPho AND ks.soSao = @SoSao

GO
/****** Object:  Table [dbo].[DatPhong]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DatPhong](
	[maDP] [char](10) NOT NULL,
	[maPhong] [char](10) NULL,
	[maKH] [char](10) NULL,
	[ngayBatDau] [date] NULL,
	[ngayTraPhong] [date] NULL,
	[ngayDat] [date] NULL,
	[donGia] [float] NULL,
	[moTa] [nvarchar](50) NULL,
	[tinhTrang] [nvarchar](50) NULL,
 CONSTRAINT [PK_DatPhong] PRIMARY KEY CLUSTERED 
(
	[maDP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HoaDon]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HoaDon](
	[maHD] [char](10) NOT NULL,
	[ngayThanhToan] [date] NULL,
	[tongTien] [float] NULL,
	[maDP] [char](10) NOT NULL,
 CONSTRAINT [PK_HoaDon] PRIMARY KEY CLUSTERED 
(
	[maHD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KhachHang]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KhachHang](
	[maKH] [char](10) NOT NULL,
	[hoTen] [nvarchar](50) NULL,
	[tenDangNhap] [char](20) NOT NULL,
	[matKhau] [char](50) NULL,
	[soCMND] [char](20) NOT NULL,
	[diaChi] [nvarchar](50) NULL,
	[soDienThoai] [char](20) NULL,
	[moTa] [nvarchar](50) NULL,
	[email] [char](50) NOT NULL,
 CONSTRAINT [PK_KhachHang] PRIMARY KEY CLUSTERED 
(
	[maKH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KhachSan]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[KhachSan](
	[maKS] [char](10) NOT NULL,
	[tenKS] [nvarchar](50) NOT NULL,
	[soSao] [float] NULL,
	[soNha] [char](10) NULL,
	[duong] [nvarchar](50) NULL,
	[quan] [nvarchar](20) NULL,
	[thanhPho] [nvarchar](20) NULL,
	[giaTB] [float] NULL,
	[moTa] [nvarchar](50) NULL,
 CONSTRAINT [PK_KhachSan] PRIMARY KEY CLUSTERED 
(
	[maKS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LoaiPhong]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LoaiPhong](
	[maLoaiPhong] [char](10) NOT NULL,
	[tenLoaiPhong] [nvarchar](50) NOT NULL,
	[maKS] [char](10) NOT NULL,
	[donGia] [float] NULL,
	[moTa] [nvarchar](50) NULL,
	[slTrong] [int] NULL,
 CONSTRAINT [PK_LoaiPhong] PRIMARY KEY CLUSTERED 
(
	[maLoaiPhong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Phong]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Phong](
	[maPhong] [char](10) NOT NULL,
	[loaiPhong] [char](10) NOT NULL,
	[soPhong] [int] NULL,
 CONSTRAINT [PK_Phong] PRIMARY KEY CLUSTERED 
(
	[maPhong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TrangThaiPhong]    Script Date: 12/28/2015 7:01:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrangThaiPhong](
	[maPhong] [char](10) NOT NULL,
	[ngay] [date] NOT NULL,
	[tinhTrang] [nvarchar](50) NULL,
 CONSTRAINT [PK_TrangThaiPhong] PRIMARY KEY CLUSTERED 
(
	[maPhong] ASC,
	[ngay] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[DatPhong] ([maDP], [maPhong], [maKH], [ngayBatDau], [ngayTraPhong], [ngayDat], [donGia], [moTa], [tinhTrang]) VALUES (N'NV1132630 ', N'NVCB1     ', N'1312515   ', CAST(0xD53A0B00 AS Date), CAST(0xD93A0B00 AS Date), CAST(0xD43A0B00 AS Date), 2500, N'', N'chưa xác nhận')
INSERT [dbo].[DatPhong] ([maDP], [maPhong], [maKH], [ngayBatDau], [ngayTraPhong], [ngayDat], [donGia], [moTa], [tinhTrang]) VALUES (N'NVCB2     ', N'NVCB2     ', N'1312515   ', CAST(0xD43A0B00 AS Date), CAST(0xD73A0B00 AS Date), CAST(0xD43A0B00 AS Date), 2500, N'', N'chưa xác nhận')
INSERT [dbo].[DatPhong] ([maDP], [maPhong], [maKH], [ngayBatDau], [ngayTraPhong], [ngayDat], [donGia], [moTa], [tinhTrang]) VALUES (N'NVCB4     ', N'NVCB4     ', N'1312515   ', CAST(0xD43A0B00 AS Date), CAST(0xD73A0B00 AS Date), CAST(0xD43A0B00 AS Date), 2500, N'', N'chưa xác nhận')
INSERT [dbo].[DatPhong] ([maDP], [maPhong], [maKH], [ngayBatDau], [ngayTraPhong], [ngayDat], [donGia], [moTa], [tinhTrang]) VALUES (N'NVCB4131  ', N'NVCB4     ', N'1312515   ', CAST(0xCD3A0B00 AS Date), CAST(0xCF3A0B00 AS Date), CAST(0xCC3A0B00 AS Date), 2300, NULL, N'đã xác nhận')
INSERT [dbo].[DatPhong] ([maDP], [maPhong], [maKH], [ngayBatDau], [ngayTraPhong], [ngayDat], [donGia], [moTa], [tinhTrang]) VALUES (N'NVCT1     ', N'NVCT1     ', N'1312515   ', CAST(0xD43A0B00 AS Date), CAST(0xD73A0B00 AS Date), CAST(0xD43A0B00 AS Date), 3200, N'', N'chưa xác nhận')
INSERT [dbo].[DatPhong] ([maDP], [maPhong], [maKH], [ngayBatDau], [ngayTraPhong], [ngayDat], [donGia], [moTa], [tinhTrang]) VALUES (N'NVCT2     ', N'NVCT2     ', N'nat272    ', CAST(0xD43A0B00 AS Date), CAST(0xD93A0B00 AS Date), CAST(0xD43A0B00 AS Date), 3200, N'', N'chưa xác nhận')
INSERT [dbo].[DatPhong] ([maDP], [maPhong], [maKH], [ngayBatDau], [ngayTraPhong], [ngayDat], [donGia], [moTa], [tinhTrang]) VALUES (N'NVCT3     ', N'NVCT3     ', N'nat272    ', CAST(0xD43A0B00 AS Date), CAST(0xD93A0B00 AS Date), CAST(0xD43A0B00 AS Date), 3200, N'', N'chưa xác nhận')
INSERT [dbo].[DatPhong] ([maDP], [maPhong], [maKH], [ngayBatDau], [ngayTraPhong], [ngayDat], [donGia], [moTa], [tinhTrang]) VALUES (N'NVCT4131  ', N'NVCT4     ', N'1312515   ', CAST(0xCD3A0B00 AS Date), CAST(0xCF3A0B00 AS Date), CAST(0xCC3A0B00 AS Date), 3500, NULL, N'đã xác nhận')
INSERT [dbo].[KhachHang] ([maKH], [hoTen], [tenDangNhap], [matKhau], [soCMND], [diaChi], [soDienThoai], [moTa], [email]) VALUES (N'1312020   ', N'Bảo Ân', N'baoan2020           ', N'baoan2020                                         ', N'131202000           ', N'NVC', N'0321456789          ', NULL, N'baoan1995@gmail.com                               ')
INSERT [dbo].[KhachHang] ([maKH], [hoTen], [tenDangNhap], [matKhau], [soCMND], [diaChi], [soDienThoai], [moTa], [email]) VALUES (N'1312515   ', N'Diệp Sĩ Thanh', N'ds.thanh95          ', N'sithanh                                           ', N'272497744           ', N'1133 THD', N'0968831427          ', NULL, N'ds.thanh95@gmail.com                              ')
INSERT [dbo].[KhachHang] ([maKH], [hoTen], [tenDangNhap], [matKhau], [soCMND], [diaChi], [soDienThoai], [moTa], [email]) VALUES (N'1312543   ', N'Cao Minh Thế', N'the1312543          ', N'minhthe                                           ', N'1312754300          ', N'NVC', N'0987654321          ', NULL, N'minhthe@gmail.com                                 ')
INSERT [dbo].[KhachHang] ([maKH], [hoTen], [tenDangNhap], [matKhau], [soCMND], [diaChi], [soDienThoai], [moTa], [email]) VALUES (N'1312720   ', N'Khánh Duy', N'khanhduy            ', N'duy1312720                                        ', N'131272000           ', N'NVC', N'0169490590          ', NULL, N'khanhduy@gmail.com                                ')
INSERT [dbo].[KhachHang] ([maKH], [hoTen], [tenDangNhap], [matKhau], [soCMND], [diaChi], [soDienThoai], [moTa], [email]) VALUES (N'1312724   ', N'Đoàn Thị Hồng Hân', N'han1312724          ', N'honghan                                           ', N'131272400           ', N'NVC', N'0123456789          ', NULL, N'han@gmail.com                                     ')
INSERT [dbo].[KhachHang] ([maKH], [hoTen], [tenDangNhap], [matKhau], [soCMND], [diaChi], [soDienThoai], [moTa], [email]) VALUES (N'1312736   ', N'Nguyễn Hoàng Phương', N'phuonghoang         ', N'phuonghoang                                       ', N'131273600           ', N'NVC', N'0147258369          ', NULL, N'phuong@gmail.com                                  ')
INSERT [dbo].[KhachHang] ([maKH], [hoTen], [tenDangNhap], [matKhau], [soCMND], [diaChi], [soDienThoai], [moTa], [email]) VALUES (N'nat272    ', N'Nate', N'natekiller10        ', N'sithanh                                           ', N'272497745           ', N'1133 Trần Hưng Đạo Phường 5 Quận 5', N'0968831427          ', N'', N'natekiller10@gmail.c                              ')
INSERT [dbo].[KhachHang] ([maKH], [hoTen], [tenDangNhap], [matKhau], [soCMND], [diaChi], [soDienThoai], [moTa], [email]) VALUES (N'nat274    ', N'Nate', N'natekiller11        ', N'sithanh                                           ', N'274497746           ', N'1133 Trần Hưng Đạo Phường 5 Quận 5', N'0968831429          ', N'', N'natekiller11@gmail.c                              ')
INSERT [dbo].[KhachSan] ([maKS], [tenKS], [soSao], [soNha], [duong], [quan], [thanhPho], [giaTB], [moTa]) VALUES (N'CT        ', N'Cao Thắng', 2, N'22        ', N'Cao Thắng', N'2', N'Hồ Chí Minh', 3100, NULL)
INSERT [dbo].[KhachSan] ([maKS], [tenKS], [soSao], [soNha], [duong], [quan], [thanhPho], [giaTB], [moTa]) VALUES (N'HTH       ', N'Hồ Thị Hương', 5, N'37        ', N'Hồ Thị Hương', N'4', N'Hồ Chí Minh', 3100, NULL)
INSERT [dbo].[KhachSan] ([maKS], [tenKS], [soSao], [soNha], [duong], [quan], [thanhPho], [giaTB], [moTa]) VALUES (N'HV        ', N'Hùng Vương', 5, N'15        ', N'Hùng Vương', N'2', N'Hà Nội', 4700, NULL)
INSERT [dbo].[KhachSan] ([maKS], [tenKS], [soSao], [soNha], [duong], [quan], [thanhPho], [giaTB], [moTa]) VALUES (N'LTT       ', N'Lý Thái Tổ', 5, N'147       ', N'Lý Thái Tổ', N'5', N'Hồ Chí Minh', 2300, NULL)
INSERT [dbo].[KhachSan] ([maKS], [tenKS], [soSao], [soNha], [duong], [quan], [thanhPho], [giaTB], [moTa]) VALUES (N'NDC       ', N'Nguyễn Đình Chiểu', 3, N'31        ', N'Nguyễn Đình Chiểu', N'2', N'Hồ Chí Minh', 2300, NULL)
INSERT [dbo].[KhachSan] ([maKS], [tenKS], [soSao], [soNha], [duong], [quan], [thanhPho], [giaTB], [moTa]) VALUES (N'NT        ', N'Nguyễn Trải', 4, N'235       ', N'Nguyễn Trải', N'3', N'Hà Nội', 3200, NULL)
INSERT [dbo].[KhachSan] ([maKS], [tenKS], [soSao], [soNha], [duong], [quan], [thanhPho], [giaTB], [moTa]) VALUES (N'NVC       ', N'Nguyễn Văn Cừ', 1, N'243       ', N'Nguyễn Văn Cừ', N'5', N'Hồ Chí Minh', 5000, NULL)
INSERT [dbo].[KhachSan] ([maKS], [tenKS], [soSao], [soNha], [duong], [quan], [thanhPho], [giaTB], [moTa]) VALUES (N'SVH       ', N'Sư Vạn Hạnh', 3, N'123       ', N'Sư Vạn hạnh', N'5', N'Hồ Chí Minh', 3500, NULL)
INSERT [dbo].[KhachSan] ([maKS], [tenKS], [soSao], [soNha], [duong], [quan], [thanhPho], [giaTB], [moTa]) VALUES (N'TD        ', N'Tôn Đản', 2, N'42        ', N'Tôn Đản', N'2', N'Hồ Chí Minh', 1800, NULL)
INSERT [dbo].[KhachSan] ([maKS], [tenKS], [soSao], [soNha], [duong], [quan], [thanhPho], [giaTB], [moTa]) VALUES (N'THD       ', N'Trần Hưng Đạo', 1, N'23        ', N'Trần Hưng Đạo', N'1', N'Hồ Chí Minh', 3600, NULL)
INSERT [dbo].[LoaiPhong] ([maLoaiPhong], [tenLoaiPhong], [maKS], [donGia], [moTa], [slTrong]) VALUES (N'LTTB      ', N'LTTB', N'LTT       ', 2300, NULL, 2)
INSERT [dbo].[LoaiPhong] ([maLoaiPhong], [tenLoaiPhong], [maKS], [donGia], [moTa], [slTrong]) VALUES (N'LTTT      ', N'LTTT', N'LTT       ', 3600, NULL, 3)
INSERT [dbo].[LoaiPhong] ([maLoaiPhong], [tenLoaiPhong], [maKS], [donGia], [moTa], [slTrong]) VALUES (N'NVCB      ', N'NVCB', N'NVC       ', 2500, NULL, 2)
INSERT [dbo].[LoaiPhong] ([maLoaiPhong], [tenLoaiPhong], [maKS], [donGia], [moTa], [slTrong]) VALUES (N'NVCT      ', N'NVCT', N'NVC       ', 3200, NULL, 3)
INSERT [dbo].[LoaiPhong] ([maLoaiPhong], [tenLoaiPhong], [maKS], [donGia], [moTa], [slTrong]) VALUES (N'SVHB      ', N'SVHB', N'SVH       ', 2300, NULL, 2)
INSERT [dbo].[LoaiPhong] ([maLoaiPhong], [tenLoaiPhong], [maKS], [donGia], [moTa], [slTrong]) VALUES (N'SVHT      ', N'SVHT', N'SVH       ', 4500, NULL, 3)
INSERT [dbo].[LoaiPhong] ([maLoaiPhong], [tenLoaiPhong], [maKS], [donGia], [moTa], [slTrong]) VALUES (N'TDT       ', N'TDT', N'TD        ', 1100, NULL, 3)
INSERT [dbo].[LoaiPhong] ([maLoaiPhong], [tenLoaiPhong], [maKS], [donGia], [moTa], [slTrong]) VALUES (N'TDTB      ', N'TDTB', N'TD        ', 900, NULL, 2)
INSERT [dbo].[LoaiPhong] ([maLoaiPhong], [tenLoaiPhong], [maKS], [donGia], [moTa], [slTrong]) VALUES (N'THDB      ', N'THDB', N'THD       ', 3100, NULL, 2)
INSERT [dbo].[LoaiPhong] ([maLoaiPhong], [tenLoaiPhong], [maKS], [donGia], [moTa], [slTrong]) VALUES (N'THDT      ', N'THDT', N'THD       ', 3600, NULL, 3)
INSERT [dbo].[Phong] ([maPhong], [loaiPhong], [soPhong]) VALUES (N'NVCB1     ', N'NVCB      ', 1)
INSERT [dbo].[Phong] ([maPhong], [loaiPhong], [soPhong]) VALUES (N'NVCB2     ', N'NVCB      ', 2)
INSERT [dbo].[Phong] ([maPhong], [loaiPhong], [soPhong]) VALUES (N'NVCB3     ', N'NVCB      ', 3)
INSERT [dbo].[Phong] ([maPhong], [loaiPhong], [soPhong]) VALUES (N'NVCB4     ', N'NVCB      ', 4)
INSERT [dbo].[Phong] ([maPhong], [loaiPhong], [soPhong]) VALUES (N'NVCT1     ', N'NVCT      ', 1)
INSERT [dbo].[Phong] ([maPhong], [loaiPhong], [soPhong]) VALUES (N'NVCT2     ', N'NVCT      ', 2)
INSERT [dbo].[Phong] ([maPhong], [loaiPhong], [soPhong]) VALUES (N'NVCT3     ', N'NVCT      ', 3)
INSERT [dbo].[Phong] ([maPhong], [loaiPhong], [soPhong]) VALUES (N'NVCT4     ', N'NVCT      ', 4)
INSERT [dbo].[TrangThaiPhong] ([maPhong], [ngay], [tinhTrang]) VALUES (N'NVCB1     ', CAST(0xD43A0B00 AS Date), N'còn trống')
INSERT [dbo].[TrangThaiPhong] ([maPhong], [ngay], [tinhTrang]) VALUES (N'NVCB2     ', CAST(0xD43A0B00 AS Date), N'còn trống')
INSERT [dbo].[TrangThaiPhong] ([maPhong], [ngay], [tinhTrang]) VALUES (N'NVCB3     ', CAST(0xD33A0B00 AS Date), N'còn trống')
INSERT [dbo].[TrangThaiPhong] ([maPhong], [ngay], [tinhTrang]) VALUES (N'NVCB4     ', CAST(0xD43A0B00 AS Date), N'còn trống')
INSERT [dbo].[TrangThaiPhong] ([maPhong], [ngay], [tinhTrang]) VALUES (N'NVCT1     ', CAST(0xD43A0B00 AS Date), N'còn trống')
INSERT [dbo].[TrangThaiPhong] ([maPhong], [ngay], [tinhTrang]) VALUES (N'NVCT2     ', CAST(0xD43A0B00 AS Date), N'còn trống')
INSERT [dbo].[TrangThaiPhong] ([maPhong], [ngay], [tinhTrang]) VALUES (N'NVCT3     ', CAST(0xD43A0B00 AS Date), N'còn trống')
INSERT [dbo].[TrangThaiPhong] ([maPhong], [ngay], [tinhTrang]) VALUES (N'NVCT4     ', CAST(0xD43A0B00 AS Date), N'còn trống')
ALTER TABLE [dbo].[DatPhong]  WITH CHECK ADD  CONSTRAINT [FK_DatPhong_KhachHang] FOREIGN KEY([maKH])
REFERENCES [dbo].[KhachHang] ([maKH])
GO
ALTER TABLE [dbo].[DatPhong] CHECK CONSTRAINT [FK_DatPhong_KhachHang]
GO
ALTER TABLE [dbo].[DatPhong]  WITH CHECK ADD  CONSTRAINT [FK_DatPhong_Phong] FOREIGN KEY([maPhong])
REFERENCES [dbo].[Phong] ([maPhong])
GO
ALTER TABLE [dbo].[DatPhong] CHECK CONSTRAINT [FK_DatPhong_Phong]
GO
ALTER TABLE [dbo].[HoaDon]  WITH CHECK ADD  CONSTRAINT [FK_HoaDon_DatPhong] FOREIGN KEY([maDP])
REFERENCES [dbo].[DatPhong] ([maDP])
GO
ALTER TABLE [dbo].[HoaDon] CHECK CONSTRAINT [FK_HoaDon_DatPhong]
GO
ALTER TABLE [dbo].[LoaiPhong]  WITH CHECK ADD  CONSTRAINT [FK_LoaiPhong_KhachSan] FOREIGN KEY([maKS])
REFERENCES [dbo].[KhachSan] ([maKS])
GO
ALTER TABLE [dbo].[LoaiPhong] CHECK CONSTRAINT [FK_LoaiPhong_KhachSan]
GO
ALTER TABLE [dbo].[Phong]  WITH CHECK ADD  CONSTRAINT [FK_Phong_LoaiPhong] FOREIGN KEY([loaiPhong])
REFERENCES [dbo].[LoaiPhong] ([maLoaiPhong])
GO
ALTER TABLE [dbo].[Phong] CHECK CONSTRAINT [FK_Phong_LoaiPhong]
GO
ALTER TABLE [dbo].[TrangThaiPhong]  WITH CHECK ADD  CONSTRAINT [FK_TrangThaiPhong_Phong] FOREIGN KEY([maPhong])
REFERENCES [dbo].[Phong] ([maPhong])
GO
ALTER TABLE [dbo].[TrangThaiPhong] CHECK CONSTRAINT [FK_TrangThaiPhong_Phong]
GO
ALTER TABLE [dbo].[DatPhong]  WITH CHECK ADD  CONSTRAINT [CK_DatPhong_tinhTrang] CHECK  (([tinhTrang]=N'chưa xác nhận' OR [tinhTrang]=N'đã xác nhận'))
GO
ALTER TABLE [dbo].[DatPhong] CHECK CONSTRAINT [CK_DatPhong_tinhTrang]
GO
ALTER TABLE [dbo].[TrangThaiPhong]  WITH CHECK ADD  CONSTRAINT [CK_TrangThaiPhong_tinhTrang] CHECK  (([tinhTrang]=N'đang sử dụng' OR [tinhTrang]=N'đang bảo trì' OR [tinhTrang]=N'còn trống'))
GO
ALTER TABLE [dbo].[TrangThaiPhong] CHECK CONSTRAINT [CK_TrangThaiPhong_tinhTrang]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TrangThaiPhong', @level2type=N'CONSTRAINT',@level2name=N'CK_TrangThaiPhong_tinhTrang'
GO
USE [master]
GO
ALTER DATABASE [QLKS] SET  READ_WRITE 
GO
